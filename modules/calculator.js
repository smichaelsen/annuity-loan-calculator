export function _getDueSpecialPayments(specialPayments, currentDate) {
    return specialPayments.filter((payment) => new Date(payment.date) <= currentDate);
}

export function _handleSpecialPayments(specialPayments, repaymentPlan, remainingLoan) {
    specialPayments.forEach((specialPayment) => {
        const remainingSpecialPayment = specialPayment.amount;
        const specialRepayment = {
            date: specialPayment.date,
            remainingLoan: remainingLoan - remainingSpecialPayment,
            monthlyPayment: specialPayment.amount,
            principalPaid: specialPayment.amount,
            interestPaid: .0,
            totalInterestPaid: _sumInterestPaid(repaymentPlan),
        };
        repaymentPlan.push(specialRepayment);
    });
    return repaymentPlan;
}

export function _sumInterestPaid(repaymentPlan) {
    return repaymentPlan.reduce((sum, repayment) => sum + parseFloat(repayment.interestPaid), 0);
}

export function calculateLoanRepayment(
    startDate,
    loanAmount,
    interestRate,
    monthlyInstallment,
    specialPayments,
    installmentAdjustments = []
) {
    // Convert the interest rate to a decimal
    const monthlyInterestRate = parseFloat(interestRate) / 100 / 12;

    // Convert monthlyInstallment to a float
    let monthlyPayment = parseFloat(monthlyInstallment);

    // Initialize variables for calculations
    let remainingLoan = parseFloat(loanAmount);
    let totalInterestPaid = 0;
    let repaymentPlan = [];

    // Sort special payments and installmentAdjustments by date in ascending order
    specialPayments.sort((a, b) => new Date(a.date) - new Date(b.date));
    installmentAdjustments.sort((a, b) => new Date(a.date) - new Date(b.date));

    // Initialize special payment index and adjustment index
    let specialPaymentIndex = 0;
    let adjustmentIndex = 0;

    let currentDate = startDate;
    while (remainingLoan > 0) {
        // apply adjustments
        const dueInstallmentAdjustments = installmentAdjustments.slice(adjustmentIndex).filter((adjustment) => new Date(adjustment.date) <= currentDate);
        if (dueInstallmentAdjustments.length > 0) {
            monthlyPayment = parseFloat(dueInstallmentAdjustments[dueInstallmentAdjustments.length - 1].amount);
        }
        adjustmentIndex += dueInstallmentAdjustments.length;

        // apply special payments
        const dueSpecialPayments = _getDueSpecialPayments(specialPayments.slice(specialPaymentIndex), currentDate);
        repaymentPlan = _handleSpecialPayments(dueSpecialPayments, repaymentPlan, remainingLoan);
        specialPaymentIndex += dueSpecialPayments.length;
        // Read remaining loan from the last payment plan entry
        if (repaymentPlan.length > 0) {
            remainingLoan = parseFloat(repaymentPlan[repaymentPlan.length - 1].remainingLoan);
        }

        // apply regular monthly payments
        const monthlyInterestPayment = remainingLoan * monthlyInterestRate;
        const thisMonthsPayment = Math.min(monthlyPayment, remainingLoan + monthlyInterestPayment);
        let principalPaid = thisMonthsPayment - monthlyInterestPayment;

        if (principalPaid <= 0) {
            // this would lead to an infinite loop
            throw new Error('Monthly payment is too low.');
        }

        remainingLoan -= principalPaid;
        totalInterestPaid += monthlyInterestPayment;

        const repayment = {
            date: new Date(currentDate.getFullYear(), currentDate.getMonth(), 1),
            remainingLoan: remainingLoan,
            monthlyPayment: thisMonthsPayment,
            principalPaid: principalPaid,
            interestPaid: monthlyInterestPayment,
            totalInterestPaid: totalInterestPaid,
        };
        repaymentPlan.push(repayment);

        currentDate.setMonth(currentDate.getMonth() + 1);
    }

    return repaymentPlan;
}
