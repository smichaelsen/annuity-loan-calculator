// Function to format a number as currency with thousands separators and a currency sign
export function formatCurrency(number, currencySign = '€') {
    // Ensure that the number is a valid number
    if (typeof number !== 'number' || isNaN(number)) {
        throw new Error('Invalid number');
    }

    // Convert the number to a string with two decimal places
    const formattedNumber = number.toFixed(2);

    // Split the number into integer and decimal parts
    const [integerPart, decimalPart] = formattedNumber.split('.');

    // Add thousands separators to the integer part
    const integerWithSeparators = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, '.');

    // Combine the integer part, decimal part, and currency sign
    const formattedCurrency = `${integerWithSeparators},${decimalPart}${currencySign}`;

    return formattedCurrency;
}
