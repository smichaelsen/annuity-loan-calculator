import { _getDueSpecialPayments, _handleSpecialPayments, _sumInterestPaid } from './calculator.js';

describe('_getDueSpecialPayments', () => {
    it('should return an empty set if there are no special payments', () => {
        const specialPayments = [];
        const currentDate = new Date('2020-01-01');
        const result = _getDueSpecialPayments(specialPayments, currentDate);
        expect(result).toEqual([]);
    });

    it('should return an empty set if there are no due special payments', () => {
        const specialPayments = [
            { date: '2020-01-02', amount: 100 },
            { date: '2020-01-03', amount: 200 },
        ];
        const currentDate = new Date('2020-01-01');
        const result = _getDueSpecialPayments(specialPayments, currentDate);
        expect(result).toEqual([]);
    });

    it('should return a single special payment if there is one due special payment', () => {
        const specialPayments = [
            { date: '2020-01-01', amount: 100 },
            { date: '2020-01-02', amount: 200 },
        ];
        const currentDate = new Date('2020-01-01');
        const result = _getDueSpecialPayments(specialPayments, currentDate);
        expect(result).toEqual([{ date: '2020-01-01', amount: 100 }]);
    });

    it('should return multiple special payments if there are multiple due special payments', () => {
        const specialPayments = [
            { date: '2020-01-01', amount: 100 },
            { date: '2020-01-02', amount: 200 },
            { date: '2020-01-03', amount: 200 },
        ];
        const currentDate = new Date('2020-01-02');
        const result = _getDueSpecialPayments(specialPayments, currentDate);
        expect(result).toEqual([
            { date: '2020-01-01', amount: 100 },
            { date: '2020-01-02', amount: 200 },
        ]);
    });
});

describe('_handleSpecialPayments', () => {
    it('should return the repayment plan unchanged if there are no special payments', () => {
        const specialPayments = [];
        const repaymentPlan = [
            {
                date: new Date('2020-01-01'),
                remainingLoan: 1000.0,
                monthlyPayment: 100.0,
                principalPaid: 80.0,
                interestPaid: 20.0,
                totalInterestPaid: 20.0,
            },
            {
                date: new Date('2020-02-01'),
                remainingLoan: 920.0,
                monthlyPayment: 100.0,
                principalPaid: 80.0,
                interestPaid: 20.0,
                totalInterestPaid: 40.0,
            },
        ];
        const result = _handleSpecialPayments(specialPayments, repaymentPlan);
        expect(result).toEqual(repaymentPlan);
    });

    it('should add a single special payment to the repayment plan', () => {
        const specialPayments = [
            { date: new Date('2020-01-02'), amount: 100 },
        ];
        const repaymentPlan = [
            {
                date: new Date('2020-01-01'),
                remainingLoan: 1000.0,
                monthlyPayment: 100.0,
                principalPaid: 80.0,
                interestPaid: 20.0,
                totalInterestPaid: 20.0,
            },
        ];
        const result = _handleSpecialPayments(specialPayments, repaymentPlan, 1000.0);
        expect(result.length).toEqual(2);
        const latestRepayment = result[result.length - 1];
        expect(latestRepayment.date).toEqual(new Date('2020-01-02'));
        expect(latestRepayment.remainingLoan).toEqual(900.0);
        expect(latestRepayment.monthlyPayment).toEqual(100.0);
        expect(latestRepayment.principalPaid).toEqual(100.0);
        expect(latestRepayment.interestPaid).toEqual(.0);
        expect(latestRepayment.totalInterestPaid).toEqual(20.0);
    });
});

describe('_sumInterestPaid', () => {
    it('should return 0 if there are no repayments', () => {
        const repayments = [];
        const result = _sumInterestPaid(repayments);
        expect(result).toEqual(0);
    });

    it('should return the interest paid for a single repayment', () => {
        const repayments = [
            {
                date: new Date('2020-01-01'),
                remainingLoan: 1000.0,
                monthlyPayment: 100.0,
                principalPaid: 80.0,
                interestPaid: 20.0,
                totalInterestPaid: 20.0,
            },
        ];
        const result = _sumInterestPaid(repayments);
        expect(result).toEqual(20.0);
    });

    it('should return the sum of interest paid for multiple repayments', () => {
        const repayments = [
            {
                date: new Date('2020-01-01'),
                remainingLoan: 1000.0,
                monthlyPayment: 100.0,
                principalPaid: 80.0,
                interestPaid: 20.0,
                totalInterestPaid: 20.0,
            },
            {
                date: new Date('2020-02-01'),
                remainingLoan: 920.0,
                monthlyPayment: 100.0,
                principalPaid: 80.0,
                interestPaid: 20.0,
                totalInterestPaid: 40.0,
            },
        ];
        const result = _sumInterestPaid(repayments);
        expect(result).toEqual(40.0);
    });
});