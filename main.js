import './components/InstallmentAdjustments.js';
import './components/LoanInputForm.js';
import './components/PaymentPlan.js';
import './components/SpecialPayments.js';
import { calculateLoanRepayment } from './modules/calculator.js';

document.addEventListener('calculate', (e) => {
    const inputForm = document.querySelector('wc-loan-input-form');
    const specialPaymentsComponent = document.querySelector('wc-special-payments');
    const installmentAdjustmentsComponent = document.querySelector('wc-installment-adjustments');
    const repaymentPlan = calculateLoanRepayment(
        inputForm.getStartDate(),
        inputForm.getLoanAmount(),
        inputForm.getInterestRate(),
        inputForm.getMonthlyInstallment(),
        specialPaymentsComponent.getSpecialPayments(),
        installmentAdjustmentsComponent.getAdjustments(),
    );
    const paymentPlanComponent = document.querySelector('wc-payment-plan');
    paymentPlanComponent.paymentPlan = repaymentPlan;
    const resultContainer = document.querySelector('#resultContainer');
    resultContainer.setAttribute('open', '');
});
