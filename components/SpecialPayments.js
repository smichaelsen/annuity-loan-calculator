const template = document.createElement('template');

template.innerHTML = `
    <style>
         input, button { 
             margin-bottom: 1rem; 
             padding: 0.5rem; 
             border: 1px solid #ccc; 
             border-radius: 0.25rem; 
             width: 100%; 
         }
    </style>
    <div>
        <h2>Add Special Payment</h2>
        <form id="specialPaymentForm">
            <div>
                <label for="paymentDate">Payment Date:</label>
                <input type="date" id="paymentDate" required>
            </div>

            <div>
                <label for="paymentAmount">Payment Amount:</label>
                <input type="number" id="paymentAmount" required>
            </div>

            <div>
                <button type="submit">Add Payment</button>
            </div>
        </form>
    </div>

    <div>
        <h2>Special Payments</h2>
        <ul id="paymentList"></ul>
    </div>
`;

class SpecialPayments extends HTMLElement {
    constructor() {
        super();

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(template.content.cloneNode(true));

        const specialPaymentForm = this._shadowRoot.querySelector('#specialPaymentForm');
        specialPaymentForm.addEventListener('submit', this._onFormSubmit.bind(this));

        // Initialize special payments data
        this._specialPayments = [];

        // Display existing special payments or load from local storage
        this._loadSpecialPayments();

        // Display existing special payments
        this._renderSpecialPayments();
    }

    _onFormSubmit(e) {
        e.preventDefault();
        const paymentDate = this._shadowRoot.querySelector('#paymentDate').value;
        const paymentAmount = this._shadowRoot.querySelector('#paymentAmount').value;

        // Add the special payment to the data
        this._specialPayments.push({
            date: new Date(paymentDate),
            amount: parseFloat(paymentAmount),
        });

        // Clear the form
        this._shadowRoot.querySelector('#paymentDate').value = '';
        this._shadowRoot.querySelector('#paymentAmount').value = '';

        // Save special payments to local storage
        this._saveSpecialPayments();

        const event = new CustomEvent('calculate', {bubbles: true, composed: true});
        this.dispatchEvent(event);

        // Display updated special payments
        this._renderSpecialPayments();
    }

    _renderSpecialPayments() {
        const paymentList = this._shadowRoot.querySelector('#paymentList');
        paymentList.innerHTML = '';

        this._specialPayments.forEach((payment, index) => {
            const listItem = document.createElement('li');
            listItem.innerHTML = `Payment ${index + 1}: Date: ${payment.date.toLocaleDateString()}, Amount: ${payment.amount.toFixed(2)} 
                <button data-index="${index}" class="remove-button">Remove</button>`;

            const removeButton = listItem.querySelector('.remove-button');
            removeButton.addEventListener('click', this._onRemoveButtonClick.bind(this));

            paymentList.appendChild(listItem);
        });
    }

    _onRemoveButtonClick(e) {
        const index = e.target.getAttribute('data-index');
        if (index !== null) {
            // Remove the special payment at the specified index
            this._specialPayments.splice(index, 1);

            // Save special payments to local storage
            this._saveSpecialPayments();

            const event = new CustomEvent('calculate', {bubbles: true, composed: true});
            this.dispatchEvent(event);

            // Display updated special payments
            this._renderSpecialPayments();
        }
    }

    _saveSpecialPayments() {
        // Save special payments to local storage as JSON
        localStorage.setItem('specialPayments', JSON.stringify(this._specialPayments));
    }

    _loadSpecialPayments() {
        // Load special payments from local storage
        const savedPayments = localStorage.getItem('specialPayments');
        if (savedPayments) {
            this._specialPayments = JSON.parse(savedPayments);
        }
        // convert each payment date to a Date object
        this._specialPayments.map((payment) => {
            payment.date = new Date(payment.date);
            return payment;
        });
    }

    getSpecialPayments() {
        return this._specialPayments;
    }
}

window.customElements.define('wc-special-payments', SpecialPayments);
