const template = document.createElement('template');

template.innerHTML = `
    <style>
         input, button { 
             margin-bottom: 1rem; 
             padding: 0.5rem; 
             border: 1px solid #ccc; 
             border-radius: 0.25rem; 
             width: 100%; 
         }
    </style>
    <form id="loanForm">
        <div>
            <label for="startDate">Start Date:</label>
            <input type="date" id="startDate" required>
        </div>

        <div>
            <label for="loanAmount">Loan Amount:</label>
            <input type="number" id="loanAmount" required>
        </div>

        <div>
            <label for="interestRate">Interest Rate (%):</label>
            <input type="text" id="interestRate" required placeholder="Enter as a decimal (e.g., 5.5)">
        </div>

        <div>
            <label for="monthlyInstallment">Monthly Installment:</label>
            <input type="text" id="monthlyInstallment" required pattern="^[0-9]+(\\.[0-9]{1,2})?$" placeholder="Enter as a decimal (e.g., 500.00)">
        <div>
            <button type="submit">Calculate</button>
        </div>
    </form>
`;

class LoanInputForm extends HTMLElement {
    constructor() {
        super();

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(template.content.cloneNode(true));

        const loanForm = this._shadowRoot.querySelector('#loanForm');
        loanForm.addEventListener('submit', this._onFormSubmit.bind(this));

        this._prefillForm();
    }

    _onFormSubmit(e) {
        e.preventDefault();
        const startDate = this._shadowRoot.querySelector('#startDate').value;
        const loanAmount = this._shadowRoot.querySelector('#loanAmount').value;
        const interestRate = this._shadowRoot.querySelector('#interestRate').value;
        const monthlyInstallment = this._shadowRoot.querySelector('#monthlyInstallment').value;

        // Store the form data in local storage
        localStorage.setItem('startDate', startDate);
        localStorage.setItem('loanAmount', loanAmount);
        localStorage.setItem('interestRate', interestRate);
        localStorage.setItem('monthlyInstallment', monthlyInstallment);

        const event = new CustomEvent('calculate', {bubbles: true, composed: true});
        this.dispatchEvent(event);
    }

    _prefillForm() {
        const startDate = localStorage.getItem('startDate');
        const loanAmount = localStorage.getItem('loanAmount');
        const interestRate = localStorage.getItem('interestRate');
        const monthlyInstallment = localStorage.getItem('monthlyInstallment');

        if (startDate) this._shadowRoot.querySelector('#startDate').value = startDate;
        if (loanAmount) this._shadowRoot.querySelector('#loanAmount').value = loanAmount;
        if (interestRate) this._shadowRoot.querySelector('#interestRate').value = interestRate;
        if (monthlyInstallment) this._shadowRoot.querySelector('#monthlyInstallment').value = monthlyInstallment;

        window.setTimeout(() => {
            if (startDate && loanAmount && interestRate && monthlyInstallment) {
                const event = new CustomEvent('calculate', {bubbles: true, composed: true});
                this.dispatchEvent(event);
            }
        }, 0);
    }

    // Getter methods to access individual input values
    getStartDate() {
        return new Date(this._shadowRoot.querySelector('#startDate').value);
    }

    getLoanAmount() {
        return this._shadowRoot.querySelector('#loanAmount').value;
    }

    getInterestRate() {
        return this._shadowRoot.querySelector('#interestRate').value;
    }

    getMonthlyInstallment() {
        return this._shadowRoot.querySelector('#monthlyInstallment').value;
    }
}

window.customElements.define('wc-loan-input-form', LoanInputForm);
