import { formatCurrency } from '../modules/utilities.js';

// Define the template for the PaymentPlan component
const template = document.createElement('template');
template.innerHTML = `
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 1rem;
        }

        th, td {
            border: 1px solid #ccc;
            padding: 0.5rem;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }
        
        .past-date {
            color: #888;
            background-color: #f2f2f2;
        }
    </style>
    <div id="summary">
        <p>Total Runtime: <span id="totalRuntime">N/A</span></p>
        <p>Total Interest Paid: <span id="totalInterest">N/A</span></p>
        <p>Total Amount Paid: <span id="totalAmountPaid">N/A</span></p>
        <p>Final Payoff Day: <span id="finalPayoffDay">N/A</span></p>
    </div>
    <table>
        <thead>
            <tr>
                <th>Date</th>
                <th>Monthly Payment</th>
                <th>Principal Paid</th>
                <th>Interest Paid</th>
                <th>Remaining Loan</th>
            </tr>
        </thead>
        <tbody id="paymentPlanBody"></tbody>
    </table>
`;

class PaymentPlan extends HTMLElement {
    constructor() {
        super();

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(template.content.cloneNode(true));

        // Initialize properties
        this._paymentPlan = [];

        // Render the payment plan
        this._renderPaymentPlan();
    }

    // Define a setter method for the payment plan property
    set paymentPlan(value) {
        this._paymentPlan = value;
        this._renderPaymentPlan();
    }

    _renderPaymentPlan() {
        const paymentPlanBody = this._shadowRoot.querySelector('#paymentPlanBody');
        paymentPlanBody.innerHTML = '';

        const currentDate = new Date(); // Get today's date, to be used for comparison

        let totalInterest = 0;
        let totalAmountPaid = 0;
        this._paymentPlan.forEach((entry) => {
            const isPastDate = entry.date < currentDate; // Check if entry's date is in the past
            totalInterest += parseFloat(entry.interestPaid);
            totalAmountPaid += parseFloat(entry.monthlyPayment);

            const row = document.createElement('tr');

            // If the date is in the past, add the past-date class to the row
            if (isPastDate) {
                row.classList.add('past-date');
            }

            row.innerHTML = `
            <td>${entry.date.toLocaleDateString()}</td>
            <td>${formatCurrency(parseFloat(entry.monthlyPayment))}</td>
            <td>${formatCurrency(parseFloat(entry.principalPaid))}</td>
            <td>${formatCurrency(parseFloat(entry.interestPaid))}</td>
            <td>${formatCurrency(parseFloat(entry.remainingLoan))}</td>
        `;
            paymentPlanBody.appendChild(row);
        });

        // Calculate total runtime
        const totalMonths = this._paymentPlan.length;
        const years = Math.floor(totalMonths / 12);
        const months = totalMonths % 12;
        const totalRuntimeText = years > 0 ? `${years} years and ${months} months` : `${months} months`;

        // Find the last entry's date for final payoff day
        const finalPayoffDay = this._paymentPlan.length > 0 ? this._paymentPlan[this._paymentPlan.length - 1].date.toLocaleDateString() : 'N/A';

        // Display the summary
        this._shadowRoot.querySelector('#totalRuntime').textContent = totalRuntimeText;
        this._shadowRoot.querySelector('#totalInterest').textContent = formatCurrency(totalInterest);
        this._shadowRoot.querySelector('#totalAmountPaid').textContent = formatCurrency(totalAmountPaid);
        this._shadowRoot.querySelector('#finalPayoffDay').textContent = finalPayoffDay;
    }
}

// Define the custom element for the PaymentPlan component
window.customElements.define('wc-payment-plan', PaymentPlan);
