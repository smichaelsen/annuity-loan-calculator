const installmentAdjustmentTemplate = document.createElement('template');

installmentAdjustmentTemplate.innerHTML = `
    <style>
         input, button { 
             margin-bottom: 1rem; 
             padding: 0.5rem; 
             border: 1px solid #ccc; 
             border-radius: 0.25rem; 
             width: 100%; 
         }
    </style>
    <div>
        <h2>Add Installment Adjustment</h2>
        <form id="adjustmentForm">
            <div>
                <label for="adjustmentDate">Adjustment Date:</label>
                <input type="date" id="adjustmentDate" required>
            </div>

            <div>
                <label for="adjustedAmount">Adjusted Amount:</label>
                <input type="number" step="any" id="adjustedAmount" required>
            </div>

            <div>
                <button type="submit">Add Adjustment</button>
            </div>
        </form>
    </div>

    <div>
        <h2>Installment Adjustments</h2>
        <ul id="adjustmentList"></ul>
    </div>
`;

class InstallmentAdjustments extends HTMLElement {
    constructor() {
        super();

        this._shadowRoot = this.attachShadow({ mode: 'open' });
        this._shadowRoot.appendChild(installmentAdjustmentTemplate.content.cloneNode(true));

        const adjustmentForm = this._shadowRoot.querySelector('#adjustmentForm');
        adjustmentForm.addEventListener('submit', this._onFormSubmit.bind(this));

        // Initialize adjustments data
        this._adjustments = [];

        // Load and display existing adjustments
        this._loadAdjustments();
        this._renderAdjustments();
    }

    _onFormSubmit(e) {
        e.preventDefault();
        const adjustmentDate = this._shadowRoot.querySelector('#adjustmentDate').value;
        const adjustedAmount = this._shadowRoot.querySelector('#adjustedAmount').value;

        // Add the adjustment to the data
        this._adjustments.push({
            date: new Date(adjustmentDate),
            amount: parseFloat(adjustedAmount),
        });

        // Clear the form
        this._shadowRoot.querySelector('#adjustmentDate').value = '';
        this._shadowRoot.querySelector('#adjustedAmount').value = '';

        // Save adjustments to local storage
        this._saveAdjustments();

        // Display updated adjustments
        this._renderAdjustments();

        const event = new CustomEvent('calculate', {bubbles: true, composed: true});
        this.dispatchEvent(event);
    }

    _renderAdjustments() {
        const adjustmentList = this._shadowRoot.querySelector('#adjustmentList');
        adjustmentList.innerHTML = '';

        this._adjustments.forEach((adjustment, index) => {
            const listItem = document.createElement('li');
            listItem.innerHTML = `Adjustment ${index + 1}: Date: ${adjustment.date.toLocaleDateString()}, Amount: ${adjustment.amount.toFixed(2)} 
                <button data-index="${index}" class="remove-button">Remove</button>`;

            const removeButton = listItem.querySelector('.remove-button');
            removeButton.addEventListener('click', this._onRemoveButtonClick.bind(this));

            adjustmentList.appendChild(listItem);
        });
    }

    _onRemoveButtonClick(e) {
        const index = e.target.getAttribute('data-index');
        if (index !== null) {
            // Remove the adjustment at the specified index
            this._adjustments.splice(index, 1);

            // Save adjustments to local storage
            this._saveAdjustments();

            // Display updated adjustments
            this._renderAdjustments();

            const event = new CustomEvent('calculate', {bubbles: true, composed: true});
            this.dispatchEvent(event);
        }
    }

    _saveAdjustments() {
        // Save adjustments to local storage as JSON
        localStorage.setItem('installmentAdjustments', JSON.stringify(this._adjustments));
    }

    _loadAdjustments() {
        // Load adjustments from local storage
        const savedAdjustments = localStorage.getItem('installmentAdjustments');
        if (savedAdjustments) {
            this._adjustments = JSON.parse(savedAdjustments);
        }
        // convert each adjustment date to a Date object
        this._adjustments.forEach((adjustment) => {
            adjustment.date = new Date(adjustment.date);
            return adjustment;
        });
    }

    getAdjustments() {
        return this._adjustments;
    }
}

window.customElements.define('wc-installment-adjustments', InstallmentAdjustments);
